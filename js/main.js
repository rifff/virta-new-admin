 $(document).ready(function(){
	$('a.item').click(function(){
		$('.item').removeClass("active");
		$(this).addClass("active");
	})

     $('.ui.accordion')  .accordion({
  	  selector: {
      trigger: 'h3, .more-info,  i.chevron'
    }
  });

     $('.ui.dropdown').dropdown();

     $('#settings').click(function(){
     	$('#settings-menu').sidebar('setting', {
	    dimPage             : false,
	    transition          : 'overlay',
		}).sidebar('show');     
	});

     $('#lang').click(function(){

     	$('#language-menu').sidebar('setting', {
	    dimPage             : false,
	    transition          : 'overlay',
		}).sidebar('show');     
	});


     $('.browse').popup({
	  	on: 'click',
	  	transition: 'slide down',
	    inline   : true,
	    hoverable: true,
	    position : 'bottom left',
	    delay: {
	    	  show: 300,
	      	hide: 400
	    }
	  });

      $('i.icon.write').click(function(event){
      	 event.preventDefault();
 		 $('.dropdown').removeClass("disabled"); 
      	 $('input[type="text"]').addClass("editable") ; 
      	 $('input[type="text"]').attr("readonly", false); 
      	 $('.reset-password, .remove').fadeIn(); 
      	 $('.login-credentials .item').css("border", "1px solid #CCC"); 	
      	 $(".actions").fadeIn();
      });



     $('.actions').click(function(event){
     	$('.actions').fadeOut();
     	$('input[type="text"]').attr("readonly", true);
     	$('input[type="text"]').removeClass("editable").fadeIn() ;   
        $('.reset-password, .remove').fadeOut(); 
      	$('.login-credentials .item').css("border", "none"); 
      	$('.dropdown').addClass("enabled"); 	
     	event.preventDefault();
     });

     $('.login-credentials .close').click(function(){
     	$(this).parent().parent().fadeOut();
     });

     /********** LOGS CALENDAR *******/

     $('.ui.slider').slider();
     $("#datepicker").datepicker({
      showOn: "both",
      buttonText: "<i class='calendar icon small'></i>"
    });
     $("#datepicker").datepicker('setDate','today');

    $('.next-day').on("click", function () {
      var date = $('#datepicker').datepicker('getDate');
      date.setTime(date.getTime() + (1000*60*60*24))
      $('#datepicker').datepicker("setDate", date);

    });

    $('.prev-day').on("click", function () {
        var date = $('#datepicker').datepicker('getDate');
        date.setTime(date.getTime() - (1000*60*60*24))
        $('#datepicker').datepicker("setDate", date);
    });


    var x=5;
    $('.table tr:gt('+x+')').hide();
       $(document).on('click', "#showMore", function(){
       $('.table tr:gt('+x+')').fadeIn();
       if('.table tr:gt('+x+')'){
        $('#showMore').html('Show less  <i class="chevron up icon center-text"></i>').attr("id", "showLess");
       }
         $(document).on('click', "#showLess", function(){
         $('.table tr:gt('+x+')').hide();
         if('.table tr:lt('+x+')'){
         $('#showLess').html('Show more  <i class="chevron down icon center-text"></i>').attr("id", "showMore");
       }
    });
    });

  $('.customers .tabular .item').tab();

   $('#station-map .tabular .item').tab();

   $('.close-station').click(function() {
       $('.company-location').removeClass('active');
       $('.station-details').animate({width: 0, marginLeft: 0}, {duration: 400});
       $('.overlap').animate({ marginLeft: 1103}, {duration: 400});
   });

    $('.company-location').click(function() {
       $('.company-location').removeClass('active');
       $(this).addClass('active');
       $('.station-details').animate({width: 951, marginLeft: 0}, {duration: 400});
       $('.overlap').animate({ marginLeft: 150}, {duration: 400});
   });

 });

       function initialize() {
        var mapProp = {
          center:new google.maps.LatLng(51.508742,-0.120850),
          zoom:5,
          mapTypeId:google.maps.MapTypeId.ROADMAP
        };
        var map=new google.maps.Map(document.getElementById("gmap"),mapProp);
      }
      google.maps.event.addDomListener(window, 'load', initialize);


